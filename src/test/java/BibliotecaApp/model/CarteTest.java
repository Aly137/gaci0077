package BibliotecaApp.model;

import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CarteTest {

    Carte c;

    @BeforeClass
    public static void setup() {
        System.out.println("before any test");
    }


    @AfterClass
    public static void teardown() {
        System.out.println("after all tests");
    }

    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("Mara");

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Arghezi");
        c.setAutori(Autori);

        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("Razboi");
        CuvinteCheie.add("Aventura");
        c.setCuvinteCheie(CuvinteCheie);


        System.out.println("Before test");
    }

    @After
    public void tearDown() throws Exception {
        c = null;

        System.out.println("After test");
    }

    @Test
    public void getTitlu() throws Exception {
        assertEquals( "Mara", c.getTitlu());
    }

    @Test
    public void setTitlu() throws Exception {
        c.setTitlu("Pacala");
        assertEquals("Pacala", c.getTitlu());
    }

    @Test
    public void setAutori() throws Exception {
        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Alecsandri");
        Autori.add("Arghezi");

        c.setAutori(Autori);

        assertEquals(Autori, c.getAutori());
    }

    @Test(timeout = 1000)
    public void getAutori()  {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Arghezi");

        assertEquals(Autori, c.getAutori());
    }

    @Test(expected = Exception.class)
    public void setAnAparitie() throws Exception {
        c.setAnAparitie("1200");
    }
}