package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {

    private CartiRepoMock carti;

    @Before
    public void setUp() throws Exception {

        carti = new CartiRepoMock();
        System.out.println("Before the test.");
    }

    @After
    public void tearDown() throws Exception {

        carti = null;
        System.out.println("After the test.");
    }

    @Test
    public void adaugaCarteTC1() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("Harry Potter and the Methods of Rationality;Some Guy;2015;The Internet;fantasy,science"));
    }

    @Test(expected = Exception.class)
    public void adaugaCarteTC2() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("A;Some Other Guy;2010;The Internet;romance, drama"));
    }

    @Test (expected = Exception.class)
    public void adaugaCarteTC3() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("Harry Potter and the Methods of Rationality;Some Guy;2000;Nemira;fantasy,science"));
    }

    @Test (expected = Exception.class)
    public void adaugaCarteTC4() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("Harry Potter and the Methods of Rationality;Some Guy;1799;The Internet;fantasy,science"));
    }

    @Test
    public void adaugaCarteTC5() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("AB;Some Guy;2010;The Internet;SciFi"));
    }

    @Test (expected = Exception.class)
    public void adaugaCarteTC6() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("Harry Potter and the Methods of Rationality;Some Guy;2010;The Inter;fantasy,science"));
    }

    @Test
    public void adaugaCarteTC7() throws  Exception {
        carti.adaugaCarte(Carte.getCarteFromString("Harry Potter and the Methods of Rationality;Some Guy;1800;The Internet;fantasy,science"));
    }

    @Test
    public void adaugaCarteTC8() throws  Exception{
    carti.adaugaCarte(Carte.getCarteFromString("Harry Potter and the Methods of Rationality;Some Guy;2018;The Internet;fantasy,science"));
    }
}