package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTestWhiteBox {

    private CartiRepoMock cartiRepoMock;

    @Before
    public void setUp() throws Exception {
        cartiRepoMock = new CartiRepoMock();
        System.out.println("Before the test.");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("After the test.");
    }

    @Test
    public void cautaCarte01() {
        List<Carte> carti = new ArrayList<Carte>();
        assertEquals(carti, cartiRepoMock.cautaCarte("Luigi"));
    }

    @Test
    public void cautaCarte02() {

        List<Carte> cartiGasite = new ArrayList<Carte>();

        Carte c = new Carte();
        c.setTitlu("Amazing adventures");

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Mario");
        c.setAutori(Autori);

        try {
            c.setAnAparitie("2002");
        } catch (Exception e) {
            e.printStackTrace();
        }

        c.setEditura("The Internet");

        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("fantasy");

        try {
            cartiRepoMock.adaugaCarte(c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        cartiGasite = cartiRepoMock.cautaCarte("Luigi");
        assertEquals(0, cartiGasite.size());
    }

    @Test
    public void cautaCarte03() {
        List<Carte> cartiGasite = new ArrayList<Carte>();

        // Prima carte
        Carte c = new Carte();
        c.setTitlu("Amazing adventures");

        ArrayList<String> Autori = new ArrayList<String>();
        Autori.add("Mario");
        c.setAutori(Autori);

        try {
            c.setAnAparitie("2002");
        } catch (Exception e) {
            e.printStackTrace();
        }

        c.setEditura("The Internet");

        ArrayList<String> CuvinteCheie = new ArrayList<String>();
        CuvinteCheie.add("fantasy");
        c.setCuvinteCheie(CuvinteCheie);

        try {
            cartiRepoMock.adaugaCarte(c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // A doua carte
        Carte cn = new Carte();
        cn.setTitlu("Even more amazing adventures");

        ArrayList<String> Autori2 = new ArrayList<String>();
        Autori2.add("Luigi");
        cn.setAutori(Autori2);

        try {
            cn.setAnAparitie("2003");
        } catch (Exception e) {
            e.printStackTrace();
        }

        cn.setEditura("The Internet");

        ArrayList<String> CuvinteCheie2 = new ArrayList<String>();
        CuvinteCheie2.add("fantasy");
        cn.setCuvinteCheie(CuvinteCheie2);

        try {
            cartiRepoMock.adaugaCarte(cn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        cartiGasite = cartiRepoMock.cautaCarte("Luigi");
        assertEquals(1, cartiGasite.size());
    }
}