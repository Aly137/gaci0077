package BibliotecaApp.util;

import BibliotecaApp.model.Carte;

public class Validator {

    // functie booleana care verifica daca stringul trimis ca parametru contine numai litere din alfabetul englez
	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z ]+");
		if(flag == false)
			throw new Exception("String invalid");
		return flag;
	}

	// functie booleana care verifica daca stringul trimis ca parametru contine numai cifre
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}

	// valideaza structura interna a cartii c trimisa ca parametru
	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getAutori()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isStringOK(c.getTitlu()))
			throw new Exception("Titlu invalid!");

		for(String s:c.getAutori()){
			if(!isStringOK(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isStringOK(s))
				throw new Exception("Cuvant/Cuvinte cheie invalid!");
		}
		if(!isNumber(c.getAnAparitie())) {
            throw new Exception("Editura invalid!");
        }

		if (c.getTitlu().length() < 2 || c.getTitlu().length() > 100) {
            throw new Exception("Title must be between 2 and 100 characters long!");
        }

		if (c.getEditura().length() < 10 || c.getEditura().length() > 50) {
            throw new Exception("Publisher must be between 10 and 50 characters long!");
        }

        if (isNumber(c.getAnAparitie()) && (Integer.parseInt(c.getAnAparitie()) < 1800
                    || Integer.parseInt(c.getAnAparitie()) > 2018)) {
                throw new Exception("Year must be between 1800 and 2018!");
        }
	}


	//Validatori? Poate!

	/* metoda care verifica daca titlul contine intre 2 si 100 de caractere
	public static boolean isLenTitlu(String s) throws Exception {
		if (s.length() >= 2 && s.length() <= 100) {
			return true;
		}
		else
			throw new Exception("Title must be between 2 and 100 characters long!");
	}*/


	/* metoda care verifica daca numele editurii contine intre 10 si 50 de caractere
	public static boolean isLenEditura(String s) throws Exception {
		if (s.length() >= 10 && s.length() <= 50) {
			return true;
		}
		else
			throw new Exception("Publisher must be between 10 and 50 characters long!");
	}*/

	/*metoda care verifica daca anul este cuprins intre 1800 si 2018
	public static boolean isAnAparitie(int an) throws Exception {
		if (an >= 1800 && an <= 2018) {
			return true;
		}
		else
			throw new Exception("Year must be between 1800 and 2018!");
	}*/

//
//	public static boolean isOKString(String s){
//		String []t = s.split(" ");
//		if(t.length==2){
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
//		}
//		return s.matches("[a-zA-Z]+");
//	}

}